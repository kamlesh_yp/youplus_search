import React, { Component } from 'react';
import './App.css';
import Header from './Header';
import Main from './Main';
import Footer from './Footer';
import { Switch, Route } from 'react-router-dom'
import Result from './Result';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };
  }

  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route exact path="/" component={Main} />
          <Route path="/result" component={Result} />
        </Switch>
        <Footer />
      </div>
    );
  }
}

