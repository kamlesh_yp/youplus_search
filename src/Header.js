import React, { Component } from 'react';
import { Link } from "react-router-dom";

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white box-shadow">
                <h5 className="my-0 mr-md-auto font-weight-normal">&nbsp;</h5>
                <nav className="my-2 my-md-0 mr-md-3">
                    <Link className="p-2 text-dark" style={{ 'textDecoration': 'none' }} to="/">Home</Link>
                    <Link className="p-2 text-dark" target="_blank" rel="noopener noreferrer" style={{ 'textDecoration': 'none' }} to="https://www.youplus.com/contact-us.html">Contact</Link>
                </nav>
            </div>)
    }
}