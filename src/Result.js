import React, { Component } from 'react';
import qs from 'query-string'

export default class Result extends Component {
    constructor(props) {
        super(props);
        this.state = {
            q: '',
            error: null,
            isLoaded: false,
            videoData: [],
            total: 0,
            pages: 0,
            currentPage: 1
        };
        this.handleCrawl = this.handleCrawl.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.handlePagination = this.handlePagination.bind(this);
    }

    componentDidMount = (e) => {
        const query = qs.parse(this.props.location.search);
        this.handleSearch(query.q, 1)
    }

    handleCrawl(event) {
        let self = this;
        let q = self.state.q;
        var data = new FormData();
        data.append("search_query", q);
        data.append("record_set", "10");
        fetch('https://crawler.yupl.us/api/v1/crawl', {
            method: 'POST',
            body: data
        })
        .then(res => res.json())
        .then(response => {
            console.log(response)
            alert("then")
        }).catch(err => {
            alert("error")
        })
    }

    handlePagination = (e) => {
        e.preventDefault();
        this.setState({ isLoaded: false })
        var pageNumber = e.target.getAttribute('val');
        this.handleSearch(this.state.q, parseInt(pageNumber, 10))
    }

    handleSearch(searchTerm, pageNumber) {
        var data = JSON.parse('{"count":200,"pages":20,"data":[{"youplusID":"","youplusSourceURL":"","sourceId":"chu-BA_g7io","sourceURL":"https://www.youtube.com/watch?v=chu-BA_g7io","sourceCountry":"","title":"Sunfeast YiPPee! Mood Masala - Chill Mood Hindi(TVC)","description":"New Sunfeast YiPPee! Mood Masala - Now choose your taste every time with 2 Masala Mix sachets. So Dude, whats your mood?","videoThumbnail":"","videoLanguage":"","videoDuration":0,"ytCategory":"Film \u0026 Animation","tags":"","channelId":"UC7XTJ2lJiVvC11SJJAmBeLg","channelTitle":"Sunfeast YiPPee!","channelThumbnail":"https://yt3.ggpht.com/-_czpYOwj-dU/AAAAAAAAAAI/AAAAAAAAAAA/a_advWVU68s/s88-c-k-no-mo-rj-c0xffffff/photo.jpg","channelSubscriberCount":42877,"publishedAt":"2017-06-23T05:16:25.000Z","viewCount":850838,"likeCount":0,"dislikeCount":0,"commentCount":0,"favouriteCount":0,"isDownloaded":false},{"youplusID":"","youplusSourceURL":"","sourceId":"BNERlkSR3cg","sourceURL":"https://www.youtube.com/watch?v=BNERlkSR3cg","sourceCountry":"","title":"Sunfeast YiPPee! Mood Masala - Happy Mood Hindi (TVC)","description":"New Sunfeast YiPPee! Mood Masala - Now choose your taste every time with 2 Masala Mix sachets. So Dude, whats your mood?","videoThumbnail":"","videoLanguage":"","videoDuration":0,"ytCategory":"Film \u0026 Animation","tags":"","channelId":"UC7XTJ2lJiVvC11SJJAmBeLg","channelTitle":"Sunfeast YiPPee!","channelThumbnail":"https://yt3.ggpht.com/-_czpYOwj-dU/AAAAAAAAAAI/AAAAAAAAAAA/a_advWVU68s/s88-c-k-no-mo-rj-c0xffffff/photo.jpg","channelSubscriberCount":42877,"publishedAt":"2017-07-21T07:23:31.000Z","viewCount":297330,"likeCount":0,"dislikeCount":0,"commentCount":0,"favouriteCount":0,"isDownloaded":false},{"youplusID":"","youplusSourceURL":"","sourceId":"_Zpol7aHkqU","sourceURL":"https://www.youtube.com/watch?v=_Zpol7aHkqU","sourceCountry":"","title":"Sunfeast YiPPee! Chinese Noodles","description":"NEW Sunfeast Yippee Chinese Masala - A unique combination of Chinese and Masala flavors giving you tasty Chinese in minutes.","videoThumbnail":"","videoLanguage":"","videoDuration":0,"ytCategory":"People \u0026 Blogs","tags":"","channelId":"UC7XTJ2lJiVvC11SJJAmBeLg","channelTitle":"Sunfeast YiPPee!","channelThumbnail":"https://yt3.ggpht.com/-_czpYOwj-dU/AAAAAAAAAAI/AAAAAAAAAAA/a_advWVU68s/s88-c-k-no-mo-rj-c0xffffff/photo.jpg","channelSubscriberCount":42877,"publishedAt":"2013-06-28T13:30:27.000Z","viewCount":163122,"likeCount":0,"dislikeCount":0,"commentCount":0,"favouriteCount":0,"isDownloaded":false},{"youplusID":"","youplusSourceURL":"","sourceId":"ZYcL7WRJCpY","sourceURL":"https://www.youtube.com/watch?v=ZYcL7WRJCpY","sourceCountry":"","title":"Sunfeast Yippee Noodles TVC - Hindi","description":"","videoThumbnail":"","videoLanguage":"","videoDuration":0,"ytCategory":"People \u0026 Blogs","tags":"","channelId":"UC0x2ez7PoWqBxk4mtvBAjKA","channelTitle":"mousefly1","channelThumbnail":"https://yt3.ggpht.com/-uhqtDXcsK24/AAAAAAAAAAI/AAAAAAAAAAA/BCIXeJXDOJs/s88-c-k-no-mo-rj-c0xffffff/photo.jpg","channelSubscriberCount":413,"publishedAt":"2014-07-30T11:55:57.000Z","viewCount":291195,"likeCount":370,"dislikeCount":110,"commentCount":5,"favouriteCount":0,"isDownloaded":false},{"youplusID":"","youplusSourceURL":"","sourceId":"_RyJEih6LbM","sourceURL":"https://www.youtube.com/watch?v=_RyJEih6LbM","sourceCountry":"","title":"Sunfeast YiPPee! Mood Masala - Crazy Mood Hindi (TVC)","description":"New Sunfeast YiPPee! Mood Masala - Now choose your taste every time with 2 Masala Mix sachets. So Dude, whats your mood?","videoThumbnail":"","videoLanguage":"","videoDuration":0,"ytCategory":"Film \u0026 Animation","tags":"","channelId":"UC7XTJ2lJiVvC11SJJAmBeLg","channelTitle":"Sunfeast YiPPee!","channelThumbnail":"https://yt3.ggpht.com/-_czpYOwj-dU/AAAAAAAAAAI/AAAAAAAAAAA/a_advWVU68s/s88-c-k-no-mo-rj-c0xffffff/photo.jpg","channelSubscriberCount":42877,"publishedAt":"2017-06-23T05:15:07.000Z","viewCount":8915578,"likeCount":0,"dislikeCount":0,"commentCount":0,"favouriteCount":0,"isDownloaded":false},{"youplusID":"","youplusSourceURL":"","sourceId":"FXNKOjFM6Zo","sourceURL":"https://www.youtube.com/watch?v=FXNKOjFM6Zo","sourceCountry":"","title":"Sunfeast YiPPee! Tricolor Pasta","description":"Enjoy Indias First Tri-Color Pasta from Sunfeast YiPPee!","videoThumbnail":"","videoLanguage":"","videoDuration":0,"ytCategory":"People \u0026 Blogs","tags":"","channelId":"UC7XTJ2lJiVvC11SJJAmBeLg","channelTitle":"Sunfeast YiPPee!","channelThumbnail":"https://yt3.ggpht.com/-_czpYOwj-dU/AAAAAAAAAAI/AAAAAAAAAAA/a_advWVU68s/s88-c-k-no-mo-rj-c0xffffff/photo.jpg","channelSubscriberCount":42877,"publishedAt":"2014-01-02T11:19:17.000Z","viewCount":474422,"likeCount":0,"dislikeCount":0,"commentCount":0,"favouriteCount":0,"isDownloaded":false},{"youplusID":"","youplusSourceURL":"","sourceId":"oowajrc7kew","sourceURL":"https://www.youtube.com/watch?v=oowajrc7kew","sourceCountry":"","title":"Sunfeast Yippee! Goodness TVC","description":"YiPPee! Noodles is Imbued with the Goodness of Natural Vegetables, Wheat Proteins and has no Artificial colors. Connect us on Facebook: www.facebook.com/SunfeastYippee Website: www.sunfeastyippee...","videoThumbnail":"","videoLanguage":"","videoDuration":0,"ytCategory":"Film \u0026 Animation","tags":"","channelId":"UC7XTJ2lJiVvC11SJJAmBeLg","channelTitle":"Sunfeast YiPPee!","channelThumbnail":"https://yt3.ggpht.com/-_czpYOwj-dU/AAAAAAAAAAI/AAAAAAAAAAA/a_advWVU68s/s88-c-k-no-mo-rj-c0xffffff/photo.jpg","channelSubscriberCount":42877,"publishedAt":"2015-05-05T13:20:55.000Z","viewCount":1367611,"likeCount":0,"dislikeCount":0,"commentCount":0,"favouriteCount":0,"isDownloaded":false},{"youplusID":"","youplusSourceURL":"","sourceId":"7nWTkIw3VKY","sourceURL":"https://www.youtube.com/watch?v=7nWTkIw3VKY","sourceCountry":"","title":"YiPPee! noodles - The Right Choice (Vox Pop)","description":"Sunfeast YiPPee! is a favourite to children and mothers alike. It is long, non-sticky, tasty and absolutely safe. Which is why YiPPee! is the right choice.","videoThumbnail":"","videoLanguage":"","videoDuration":0,"ytCategory":"Film \u0026 Animation","tags":"","channelId":"UC7XTJ2lJiVvC11SJJAmBeLg","channelTitle":"Sunfeast YiPPee!","channelThumbnail":"https://yt3.ggpht.com/-_czpYOwj-dU/AAAAAAAAAAI/AAAAAAAAAAA/a_advWVU68s/s88-c-k-no-mo-rj-c0xffffff/photo.jpg","channelSubscriberCount":42877,"publishedAt":"2015-12-21T05:11:49.000Z","viewCount":586547,"likeCount":0,"dislikeCount":0,"commentCount":0,"favouriteCount":0,"isDownloaded":false}]}');
        fetch("https://crawler.yupl.us/api/v1/relavant?search_query=" + searchTerm + "&page=" + pageNumber).then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        videoData: (result.data != null ? result.data : []),
                        total: result.count,
                        pages: result.pages,
                        q: searchTerm,
                        currentPage: pageNumber,
                    })
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        //error,
                        videoData: data.data,
                        total: data.count,
                        pages: data.pages,
                        q: searchTerm,
                        currentPage: pageNumber,
                    });
                }
            )
    }

    renderSearchResult() {
        const { error, isLoaded, videoData, pages, currentPage } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return (<div>Searching...</div>);
        } else if (videoData.length === 0) {
            return (<div>No data available. Please initiate crawling to fetch more</div>);
        } else {
            return (
                <div>
                    <div className="row">
                        <div className="col-md-12">
                            <ul className="list-unstyled">
                                {
                                    videoData.map(item => (
                                        <li className="media" style={{ 'marginBottom': '10px' }} key={item.source_id}>
                                            <img className="mr-3" src={item.channel_thumbnail !== '' ? item.channel_thumbnail : item.thumbnail_image_url} alt={item.title} style={{'maxWidth':'88px'}}/>
                                            <div className="media-body" style={{ 'textAlign': 'left' }}>
                                                <h5 className="mt-0 mb-1">{item.title} <small>({item.yt_category !== '' ? item.yt_category : 'Uncategorized'})</small></h5>
                                                <p style={{ 'fontSize': 'small' }}><b>Description</b>: {item.description.length > 0 ? item.description : 'N/A'} </p>
                                                <p style={{ 'fontSize': 'small' }}><b>Tags</b>: {item.tags.length > 0 ? item.tags : 'N/A'}</p>
                                                <p style={{ 'fontSize': 'small' }}><b>Subscribers</b>: {item.channel_subscriber_count} | <b>Likes</b>: {item.like_count}&nbsp;|&nbsp;
                                                  <b>Dislikes</b>: {item.like_count} |  <b>Comments</b>: {item.comment_count} | <b>Country</b>: {item.source_country !== '' ? item.source_country : 'N/A'}</p>
                                                <p style={{ 'fontSize': 'small' }}><a href={item.source_url} target="_blank">Source</a></p>
                                            </div>
                                        </li>
                                    ))
                                }
                            </ul>
                        </div>
                    </div>
                    <div className="row" style={{ 'paddingBottom': '75px' }}>
                        <div className="col-md-12">
                            {
                                Array.from(Array(pages), (e, i) => {
                                    if (i + 1 === currentPage) {
                                        return (<span key={i + 1}>{' '}{i + 1}{' '}</span>)
                                    } else {
                                        return (<a val={i + 1} key={i + 1} style={{ 'cursor': 'pointer', 'textDecoration': 'none' }} href="" onClick={this.handlePagination}>{' '}{i + 1}{' '}</a>)
                                    }
                                }
                                )}
                        </div>
                    </div>
                </div>
            )
        }
    }

    renderVideoModal() {
        return (
            <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content" style={{ 'width': '130%' }}>
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">Modal title</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <iframe title="video" width="560" height="315" src="https://www.youtube.com/embed/m7dSteMFhZI" frameBorder="0" allowFullScreen></iframe>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>)
    }

    render() {
        const { q, total } = this.state;
        return (
            <div className="container">
                <div className="row" style={{ 'backgroundColor': '#56B8E9', 'marginBottom': '18px', 'padding': '8px', 'borderRadius': '8px' }}>
                    <div className="col" style={{ 'textAlign': 'left', 'color': 'white' }}>Search Result for: {q} ({total} Results)</div>
                    <div className="col" style={{ 'textAlign': 'right' }}><a role="button" className="btn btn-light btn-sm float-right" onClick={this.handleCrawl}>Crawl More</a></div>
                </div>
                {this.renderSearchResult()}
                {this.renderVideoModal()}
            </div>);
    }
}