import React, { Component } from 'react';
import Logo from './youplus-search.png'
import './App.css';

export default class Main extends Component {
    constructor(props) {
        super(props);
        this.state = { value: '' };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        var self = this;
        self.props.history.push('/result?q=' + self.state.value);
    }

    render() {
        return (
            <div className="base-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center" style={{ 'marginTop': '100px' }}>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <img className="img-fluid" src={Logo} alt="Youplus Search" title="Hello Awesome" style={{ 'maxWidth': '272px' }} />
                    </div>
                    <div className="form-group" style={{ 'marginTop': '40px' }}>
                        <input type="text" value={this.state.value} onChange={this.handleChange} className="form-control" id="searchTerm" placeholder="Enter keyword to query our database" autoFocus />
                    </div>
                </form>
            </div>
        )
    }
}